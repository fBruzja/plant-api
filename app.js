import { ApiClient } from "./plant-api/src";

const apiClient = new ApiClient();

module.exports = apiClient;
