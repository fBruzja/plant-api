const childProcess = require('child_process');
const API_PATH_SPEC = 'plant-api/src'; // Path to the Angular package / Library directory
const SWAGGER_JSON_CONTAINER_PATH = `./api-docs.json`; // Path to swagger json file

class Generate {
    constructor() {
        // --additional-properties ngVersion=10, Right now this flag has no impact in the current command.
        // this.patchApiModuleForAngular10() is a temporary solution until this https://github.com/swagger-api/swagger-codegen-generators/pull/730
        // been fixed.

        this.generateServicesCommand = `java -jar openapi-generator-cli-5.1.0.jar generate \
                                       -i ${SWAGGER_JSON_CONTAINER_PATH} \
                                       -g typescript-angular \
                                       -o ${API_PATH_SPEC}`;
    }

    /**
     * Main function to generate services
     */
    generateServices() {
        this.cleanPreviousBuild();
        this.generateApiSpec();
    }

    /**
     * Clears the previously built services and models
     */
    cleanPreviousBuild() {
        removeDirectory(`${__dirname}/${API_PATH_SPEC}/src`);
    }

    /**
     * Generate swagger API spec.
     */
    generateApiSpec() {
        try {
            childProcess.execSync(this.generateServicesCommand);
            logSuccess('Services generated successfully.');
        } catch {
            logError('Service Generation Failed');
        }
    }
}

/**
 * Checks if script is running in a windows environment
 */
function isWindows() {
    return process.platform === 'win32';
}

/**
 * @param {string} path
 *
 * @return {void}
 */
function removeDirectory(path) {
    path = isWindows() ? path.replace(/\//g, '\\') : path;

    const clearCommand = isWindows() ? `if exist ${path} RMDIR ${path} /S /Q` : `rm -rf ${path}`;

    childProcess.execSync(clearCommand, {encoding: 'utf-8'});
}

/**
 * @param {string} message
 *
 * @return {void}
 */
function logError(message) {
    console.log('\x1b[31m%s\x1b[0m', message);
}

/**
 * @param {string} message
 *
 * @return {void}
 */
function logSuccess(message) {
    console.log('\x1b[32m%s\x1b[0m', message);
}

module.exports.generate = function () {
    new Generate().generateServices();
}
