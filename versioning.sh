#!/bin/bash
version='1.'$(grep version api-docs.json | cut -d ':' -f 2 | sed 's/"//g' |  tr -d '[:space:]')
sed "s/%version%/$version/" package.json.template > package.json
